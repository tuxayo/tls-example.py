import unittest

from functools import partial

import test.helpers as helpers
import tls_example.client as cli
import tls_example.server as srv
import tls_example.config as config


class Test(unittest.TestCase):

    def setUp(self):
        config.SRV_NB_CLIENT_TO_HANDLE = 1

    def test_message_with_right_auth(self):
        helpers.run_client_and_server(self,
                                      self.client_right_auth, self.server)

    def client_right_auth(test_runner):
        sock = cli.connect_to_server()
        success = cli.send_message(sock,
                                   'my_login', 'my passphrase', 'my message')
        test_runner.assertEqual(success, True)

    def test_message_with_wrong_pass(self):
        helpers.run_client_and_server(self,
                                      self.client_wrong_pass, self.server)

    def client_wrong_pass(self):
        sock = cli.connect_to_server()
        success = cli.send_message(sock,
                                   'my_login', 'WRONG passphrase', 'my message')
        self.assertEqual(success, False)

    def test_message_with_wrong_login(self):
        helpers.run_client_and_server(self,
                                      self.client_wrong_login, self.server)

    def client_wrong_login(self):
        sock = cli.connect_to_server()
        success = cli.send_message(sock,
                                   'WRONGlogin', 'my passphrase', 'my message')
        self.assertEqual(success, False)

    def test_custom_ciphersuite_1(self):
        client_with_ciphersuite_1 = partial(self.client_with_ciphersuite,
                                            cipher_suite='ECDHE-RSA-AES256-GCM-SHA384')
        helpers.run_client_and_server(self,
                                      client_with_ciphersuite_1, self.server)

    def test_custom_ciphersuite_2(self):
        client_with_ciphersuite_2 = partial(self.client_with_ciphersuite,
                                            cipher_suite='ECDHE-RSA-AES128-SHA256')
        helpers.run_client_and_server(self,
                                      client_with_ciphersuite_2, self.server)

    def client_with_ciphersuite(test_runner, cipher_suite):
        sock = cli.connect_to_server(cipher_suite=cipher_suite)
        actual_cipher_suite, _, _ = sock.cipher()
        test_runner.assertEqual(actual_cipher_suite, cipher_suite)
        success = cli.send_message(sock,
                                   'my_login', 'my passphrase', 'my message')
        test_runner.assertEqual(success, True)

    def server(self):
        srv.main()

if __name__ == "__main__":
    unittest.main()
