import random
import os
import socket
import tls_example.config as config

from multiprocessing import Process


def pid_exists(pid):
    try:
        os.kill(pid, 0)
        return True
    except OSError as err:
        # OSError could also be because of permission denied
        return err.errno == os.errno.EPERM


def choose_unused_port():
        port = random.randint(1024, 64000)
        listen_sock = socket.socket()
        try:
            listen_sock.bind(('', port))
            listen_sock.close()
            return port
        except:
            # print('TEST SUITE: port used, choosing another')  # TODO use logger
            listen_sock.close()
            return choose_unused_port()


def run_client_and_server(test_runner, client_function, server_function):
    config.PORT = choose_unused_port()

    server_process = Process(target=server_function)
    server_process.start()
    client_process = Process(target=client_function)
    client_process.start()

    pid, code = os.wait()

    if code == 0:
        # One of the process ended well, just wait for the other
        _, code = os.wait()
        test_runner .assertEqual(code, 0)
        return
    # the first process to exit had an issue
    if pid == server_process.pid:
        if pid_exists(client_process.pid):
            client_process.terminate()
        raise RuntimeError('Server had an issue, see previous stacktrace')
    else:
        if pid_exists(server_process.pid):
            server_process.terminate()
        raise RuntimeError('Client had an issue, see previous stacktrace')
