import time
import ssl
import socket
import json
import tkinter as tk

from tkinter import messagebox

import tls_example.config as config

from tls_example.network import send_in_socket


def main():

    # function nested to retrieve fields values
    def gui_send_message(useless_param_to_bind_return_key_to_entry_widget=None):
        sock = connect_to_server(cipher_suite=cipher_suite.get())
        success = send_message(sock,
                               login_entry.get(),
                               pass_entry.get(),
                               message_entry.get())
        if success:
            tk.messagebox.showinfo('Message status', 'Send successfully')
        else:
            messagebox.showinfo('Message status',
                                'Error, check your credentials')

    top = tk.Tk()
    label = tk.Label(top, text='login')
    label.pack(side=tk.LEFT)
    login_entry = tk.Entry(top)
    login_entry.pack(side=tk.LEFT)
    login_entry.bind('<Return>', gui_send_message)

    label2 = tk.Label(top, text='passphrase')
    label2.pack(side=tk.LEFT)
    pass_entry = tk.Entry(top)
    pass_entry.pack(side=tk.LEFT)
    pass_entry.bind('<Return>', gui_send_message)

    label3 = tk.Label(top, text='message')
    label3.pack(side=tk.LEFT)
    message_entry = tk.Entry(top)
    message_entry.pack(side=tk.LEFT)
    message_entry.bind('<Return>', gui_send_message)

    cipher_suite = tk.StringVar()
    cipher_suite.set('ECDHE-RSA-AES256-GCM-SHA384')
    radio_cipher_1 = tk.Radiobutton(top, text='ECDHE-RSA-AES256-GCM-SHA384',
                                    variable=cipher_suite,
                                    value='ECDHE-RSA-AES256-GCM-SHA384')
    radio_cipher_1.pack()
    radio_cipher_2 = tk.Radiobutton(top, text='ECDHE-RSA-AES128-SHA256',
                                    variable=cipher_suite,
                                    value='ECDHE-RSA-AES128-SHA256')
    radio_cipher_2.pack()

    send_button = tk.Button(top, text='send', command=gui_send_message)
    send_button.pack()

    top.mainloop()


def connect_to_server(host='localhost',
                      cipher_suite='ECDHE-RSA-AES256-GCM-SHA384'):
    tls_context = ssl.create_default_context(cafile=config.SERVER_CERTIFICATE)
    # tls_context.set_ciphers('ECDH-ECDSA-AES256-GCM-SHA384') # FAIL
    # tls_context.set_ciphers('ECDHE-ECDSA-AES256-GCM-SHA384') # FAIL
    # tls_context.set_ciphers('ECDH-ECDSA-AES256-GCM-SHA384') # FAIL
    # tls_context.set_ciphers('ECDHE-RSA-AES256-GCM-SHA384') # OK
    # tls_context.set_ciphers('ECDHE-RSA-AES128-SHA256') # OK
    tls_context.set_ciphers(cipher_suite)
    sock = tls_context.wrap_socket(socket.socket(socket.AF_INET),
                                   server_hostname=host)
    _connect_with_retry(sock, host, config.PORT)
    return sock


def send_message(socket, login, passphrase, message):
    authenticated_message = json.dumps({'login': login,
                                        'passphrase': passphrase,
                                        'message': message})
    send_in_socket(authenticated_message, socket)
    response = socket.recv(1024).decode('utf-8')
    socket.close()
    return response == 'ACK'


def _connect_with_retry(socket, host, port):
    try:
        socket.connect((host, port))
    except ConnectionRefusedError:
        print('server not found, retrying')
        time.sleep(1)
        _connect_with_retry(socket, host, port)


if __name__ == '__main__':
    main()
