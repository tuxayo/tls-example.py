import os

DATA_DIR = os.path.join(os.path.dirname(__file__), '..', 'data')

SERVER_CERTIFICATE = os.path.join(DATA_DIR, 'localhost_example_cert.pem')
SERVER_PRIVATE_KEY = os.path.join(DATA_DIR, 'localhost_example_privkey.pem')

SRV_NB_CLIENT_TO_HANDLE = 0 # sequencially, 0 == unlimited

PORT = 7070
