import ssl
import socket
import json
import itertools

import tls_example.config as config

from tls_example.network import send_in_socket

HOST = ''  # Symbolic name meaning all available interfaces
clients_auth_db = {'my_login': 'my passphrase'}


def main():
    # setup
    tls_context = setup_tls()
    listen_sock = listen()

    if config.SRV_NB_CLIENT_TO_HANDLE == 0:
        nb_clients_to_handle = itertools.count()  # infinity counter
    else:
        nb_clients_to_handle = range(config.SRV_NB_CLIENT_TO_HANDLE)

    for _ in nb_clients_to_handle:
        client_sock = wait_for_client(listen_sock, tls_context)
        _handle_client(client_sock)

    # clean
    listen_sock.close()


def _handle_client(sock):
    message_json = sock.recv(1024).decode('utf-8')
    if _check_message_authenticity(message_json):
        send_in_socket('ACK', sock)
    else:
        send_in_socket('NACK', sock)
    sock.close()


def setup_tls():
    tls_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    tls_context.load_cert_chain(certfile=config.SERVER_CERTIFICATE,
                                keyfile=config.SERVER_PRIVATE_KEY)
    # tls_context.set_ciphers('ECDH-ECDSA-AES256-GCM-SHA384') # FAIL
    # tls_context.set_ciphers('ECDHE-ECDSA-AES256-GCM-SHA384') # FAIL
    # tls_context.set_ciphers('ECDH-ECDSA-AES256-GCM-SHA384') # FAIL
    # tls_context.set_ciphers('ECDHE-RSA-AES256-GCM-SHA384') # OK
    # tls_context.set_ciphers('ECDHE-RSA-AES128-SHA256') # OK
    return tls_context


def listen():
    listen_sock = socket.socket()
    listen_sock.bind((HOST, config.PORT))
    listen_sock.listen(10)  # listen with a backlog of 10
    return listen_sock


def wait_for_client(listen_sock, tls_context):
    # print('SERVER: Waiting for client')  # TODO use logger
    client_sock, client_adress = listen_sock.accept()
    # print('SERVER: Connected by', client_adress)  # TODO use logger
    client_sock = tls_context.wrap_socket(client_sock, server_side=True)
    # print('SERVER: cipher' + str(client_sock.cipher()))  # TODO use logger
    return client_sock


def _check_message_authenticity(message_json):
    try:
        message = json.loads(message_json)
    except json.decoder.JSONDecodeError:
        return False

    login = message['login']
    return message['passphrase'] == clients_auth_db.get(login)


if __name__ == '__main__':
    main()
